const fetchUser = () =>
  fetch("/login", {
    method: "POST",
    body: JSON.stringify({
      username: "Jason Meng Li",
    }),
  }).then((res) => res.json());
export const userAPI = {
  fetchUser,
};
