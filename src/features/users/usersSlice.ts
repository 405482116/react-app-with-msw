import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { userAPI } from "./userAPI";
import type { RootState } from "../../app/store";

export const fetchUser = createAsyncThunk("user/fetchUser", async () => {
  const response = await userAPI.fetchUser();
  debugger;
  return response.data;
});
interface User {
  id: string;
  username: string;
  firstName: string;
  lastName: string;
}

interface UserState {
  info: User | undefined;
  status: "idle" | "loading" | "complete";
}

const initialState: UserState = {
  info: undefined,
  status: "idle",
};

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(fetchUser.pending, (state, action) => {
      state.status = "loading";
    });
    builder.addCase(fetchUser.fulfilled, (state, action) => {
      state.status = "complete";
      state.info = action.payload;
    });
  },
});

export const selectUser = (state: RootState) => state.user.info;
export const selectUserFetchStatus = (state: RootState) => state.user.status;

export default userSlice.reducer;
