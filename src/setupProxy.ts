//@ts-ignore
const { createProxyMiddleware } = require("http-proxy-middleware");

const proxy = {
  target: "http://localhost:4000",
  changeOrigin: true,
};
// const proxy2 = {
//   target: 'https://www.stackoverflow.com',
//   changeOrigin: true,
// }
module.exports = function (app: any) {
  debugger
  app.use("/login", createProxyMiddleware(proxy));
};
