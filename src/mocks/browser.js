import { setupWorker } from "msw";
import { handlers } from "msw-service";

export const worker = setupWorker(...handlers);
