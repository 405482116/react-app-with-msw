import React from "react";
import UserDisplay from "./features/users/UserDisplay";
import "./App.css";

function App() {
  return (
    <div className="App">
      <UserDisplay />
    </div>
  );
}

export default App;
