import { createMiddleware } from "@mswjs/http-middleware";
import express from "express";
import bodyParser from "body-parser";
import { handlers } from "./handlers";
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(createMiddleware(...handlers));

// const httpServer = createServer(...handlers);

app.listen(4000);
